const axios = require('axios');

// GitLab API endpoint and personal access token
const apiUrl = 'https://gitlab.com/api/v4';
const personalAccessToken = process.env.PAT;

// Function to fetch all the diffs for a merge request
async function fetchMergeRequestDiffs(projectId, mergeRequestId) {
    try {
        // Construct the API URL for fetching merge request diffs
        const url = `${apiUrl}/projects/${projectId}/merge_requests/${mergeRequestId}/diffs`;

        // Set up request headers including the personal access token
        const headers = {
            'PRIVATE-TOKEN': personalAccessToken
        };

        // Make the API request to fetch the merge request diffs
        const response = await axios.get(url, { headers });

        // Extract and return the diffs from the response data
        return response.data;
    } catch (error) {
        console.error('Error fetching merge request diffs:', error.response ? error.response.data : error.message);
        throw error;
    }
}

// Example usage: Fetch merge request diffs for a specific project and merge request ID
const projectId = process.env.CI_MERGE_REQUEST_PROJECT_ID;
const mergeRequestId = process.env.CI_MERGE_REQUEST_IID;

fetchMergeRequestDiffs(projectId, mergeRequestId)
    .then(diffs => {
        console.log('Merge Request Diffs:');
        diffs.forEach(diff => {
            console.log(`File: ${diff.old_path} -> ${diff.new_path}`);
            console.log(diff.diff);
        });
    })
    .catch(error => {
        console.error('Failed to fetch merge request diffs:', error.message);
    });
